Ceci est un essai, je vais push sur le repo en ligne sans commit, puis merge pour tenter de créer un conflit dans le merge request

Pour protéger une branche dans gitlab il faut aller dans les paramètres sur l'interface WEB en tant qu'admin et séléctionner protetion des branches puis indiquer que personne n'a le droit de push and merge
Proteger des branches git s'avère être utile afin d'empêcher qui que ce soit d'ajoute ou de modifier du code dans notre repo
Les merge conflicts peuvent être entrainés lors du merge si 2 utilisateurs font des modifications sur la même ligne par exemple, pour les résoudre il faut aller dans l'éditeur de gitlab et déplacer le code à travers différentes lignes manuellement

